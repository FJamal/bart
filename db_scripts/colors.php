<?php
    //configuration
    require("../includes/config.php");

    $dom = simplexml_load_file("http://api.bart.gov/api/route.aspx?cmd=routeinfo&route=all&key=MW9S-E7SL-26DU-VV8V");

    //sql to create a table
    $dbh->exec("CREATE TABLE colors (
        route VARCHAR(8),
        color VARCHAR (10)
    )");

    foreach($dom->xpath("/root/routes/route") as $route)
    {

        $color = $route->hexcolor;
        $routeid = $route->routeID;

        $routeid = strtolower(str_replace(" ", "", $routeid));

        //print("color: " . $color . "  route: " . $routeid . "\n");

        //sql to enter colors in db
        $sql = $dbh->prepare("INSERT INTO colors (route, color) VALUES (:route, :color)");
        $sql->bindValue(":route", $routeid);
        $sql->bindValue(":color", $color);
        $sql->execute();
    }
?>
