<?php
    //configuration
    require("../includes/config.php");

    $dom = simplexml_load_file("http://api.bart.gov/api/route.aspx?cmd=routeinfo&route=all&key=MW9S-E7SL-26DU-VV8V");

    foreach($dom->xpath("/root/routes/route") as $route)
    {
        $table_name = $route->routeID;
        //removing space and to lowercase
        $table_name = strtolower(str_replace(" ", "", $table_name));

        $stations_in_route = [];

        foreach($route->config->station as $stations)
        {
            array_push($stations_in_route, (string)$stations);
        }

        //creating sql statement to create a table for this route
        $dbh->exec("CREATE TABLE {$table_name}(
            id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
            stations VARCHAR(20)
        )");

        //preparing sql to fill created table
        $sql = $dbh->prepare("INSERT INTO {$table_name} (stations) VALUES (:station_name)");

        //iterating over the names of stations in array
        foreach($stations_in_route as $station)
        {
            //iserting into the table
            $sql->bindValue(":station_name", $station);
            $sql->execute();
        }
        
    }
?>
