<?php
    /*Script to load stations data in db*/
    //configuration
    require("../includes/config.php");

    /*to use later CREATE TABLE stations (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255),
    abbr VARCHAR(10),
    longitude FLOAT,
    latitude FLOAT,
    city VARCHAR(50),
    state VARCHAR(10),
    zip INT,
    address VARCHAR(255),
    county VARCHAR (100),
    INDEX(name, abbr , longitude, latitude)
    )ENGINE = INNODB;*/

    //loading the xml file from bart api
    $dom = simplexml_load_file("http://api.bart.gov/api/stn.aspx?cmd=stns&key=MW9S-E7SL-26DU-VV8V");

    $stations = [];
    foreach($dom->xpath("/root/stations/station") as $station)
    {
        $stations [] = [
            "name" => (string)$station->name,
            "abbr" => (string)$station->abbr,
            "longitude" => (double)$station->gtfs_longitude,
            "latitude" => (double)$station->gtfs_latitude,
            "address" => (string)$station->address,
            "city" => (string)$station->city,
            "county" =>(string)$station->county,
            "state" => (string)$station->state,
            "zip" => (int)$station->zipcode
        ];
    }

    //doing it via transaction to save me the time to deleting db if something goes wrong
    $dbh->beginTransaction();

    //creating db
    $dbh->query("CREATE TABLE stations (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255),
    abbr VARCHAR(10),
    longitude DOUBLE,
    latitude DOUBLE,
    address VARCHAR(255),
    city VARCHAR(255),
    county VARCHAR (100),
    state VARCHAR(10),
    zip INT,
    INDEX(name, abbr , longitude, latitude)
    )ENGINE = INNODB;");


    //preparing sql
    $sql = $dbh->prepare("INSERT INTO stations (name, abbr, longitude, latitude,
    address, city, county, state, zip) VALUES (:name, :abbr, :longi, :lati,
    :address, :city, :county, :state, :zip)");

    //filling the db
    foreach($stations as $station)
    {
        $sql->bindValue(":name", $station["name"]);
        $sql->bindValue(":abbr", $station["abbr"]);
        $sql->bindValue(":longi", $station["longitude"]);
        $sql->bindValue(":lati", $station["latitude"]);
        $sql->bindValue(":address", $station["address"]);
        $sql->bindValue(":city", $station["city"]);
        $sql->bindValue(":county", $station["county"]);
        $sql->bindValue(":state", $station["state"]);
        $sql->bindValue(":zip", $station["zip"]);

        $sql->execute();
    }

    $dbh->commit();

?>
