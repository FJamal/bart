<?php
    //configuration
    require("../includes/config.php");

    $dom = simplexml_load_file("http://api.bart.gov/api/etd.aspx?cmd=etd&orig=all&key=MW9S-E7SL-26DU-VV8V");

    $test = $_GET["station"];
    //$test = "LAKE";
    $data = [];

    foreach($dom->xpath("/root/station") as $station)
    {
        if($station->abbr == $test)
        {
            $departures = [];
            foreach($station->etd as $etd)
            {
                $destination = (string)$etd->destination;

                foreach($etd->estimate as $estimate)
                {
                    $departures [] = [
                        "destination" => $destination,
                        "time" => (string)$estimate->minutes,
                        "platform" => (string)$estimate->platform
                    ];
                }
            }
            $data["departures"] = $departures ;
        }
    }

    header("Content-type: application/json");
    print(json_encode($data));
    //print_r($data);
?>
