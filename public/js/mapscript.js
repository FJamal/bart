//to be used by everyfunction
var map;
var markers = [];
var polyline;
var info;



window.onload = function ()
{

    //var myLatlng = new google.maps.LatLng(37.773972, -122.431297 )
    var mapoptions = {
        center: {lat: 37.775362, lng: -122.417564},
        zoom: 11,
        mapTypeId:"roadmap"
    };

        map = new google.maps.Map(document.getElementById('map'), mapoptions);
}

    //getting the list from the DOM
    var routeslinks = document.getElementsByTagName("ul");
    var links = routeslinks[0];

    //setting up event handler on the routes
    links.onclick = function ()
    {
        var routename = whichroute();

        var routedata = routeinfo(routename);
        console.log(routedata);

        //to remove any old markers if they are there
        if(markers.length != 0)
        {
            //remove markers
            removemarkers();
        }

        //removing any previous polylines
        if(polyline)
        {
            polyline.setMap(null);
        }

        //adding the appropriate markers
        getmarkers(routedata);

        //adding the appropriate polylines
        addpolylines(routedata);

        /*setting up click events on each markers based on closure concept
        from
        https://stackoverflow.com/questions/45483689/add-onclick-listener-to-google-maps-markers-javascript
        */

        for(var i = 0; i < markers.length; i++)
        {
            (function (i){
                markers[i].addListener("click",function ()
                {
                    loadinfo(i, routedata);
                });
            })(i)

        }

    }









/*Function to determine which route was clicked returns a formatted
name for route to be used in url for ajax call*/
function whichroute(e)
{
    if(!e)
    {
        e = window.event;
    }

    var target = e.event || e.srcElement;
    var value = target.textContent;

    value = value.toLowerCase(value);
    value = value.replace(" ", "");

    return value;
}




/*Function that makes the appropriate ajax call */
function routeinfo (route)
{

    var data;
    var xhr;
    try
    {
        xhr = new XMLHttpRequest();
    }
    catch (e)
    {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }
    if(xhr == false)
    {
        alert("Javascript not supported")
    }

    var url = "getroute.php?routename=" + route;
    console.log(url);
    xhr.onreadystatechange = function ()
    {
        if(xhr.readyState == 4)
        {
            if(xhr.status == 200)
            {
                data = JSON.parse(xhr.responseText);

            }
        }
    }
    // making a syncronous call
    xhr.open("GET", url, false);
    xhr.send(null);

    return data;
}



/*Function that puts the markers */
function getmarkers(data)
{
    //getting the length json passed
    var length = data.length

    /*setting up a marker for each entry and skipping 0 index array as it
    refers to the color of the route*/
    for(var i = 0; i < length; i++)
    {
        if(i == 0)
        {
            continue;
        }
        var marker = new google.maps.Marker({
            position: {lat: parseFloat(data[i].latitude), lng: parseFloat(data[i].longitude)},
            animation:google.maps.Animation.DROP,
            title: data[i].name
            }
        );

        markers.push(marker);
    }

    //setting up markers, the markers length would be 1 less than that of data
    for(var i = 0; i < length - 1; i++)
    {
        markers[i].setMap(map);
    }
}




/*Function to remove markers*/
function removemarkers()
{
    var markerslength = markers.length;
    for(var i = 0; i < markerslength; i++)
    {
        markers[i].setMap(null);
    }

    markers = [];
}




/*Function that adds polylines*/
function addpolylines(data)
{

    var polylinepath = [];

    //storing the color of the route
    var color = data[0].color;

    var length = data.length;

    for(var i = 1; i < length; i++)
    {
        var coordinate ={lat: parseFloat(data[i].latitude), lng: parseFloat(data[i].longitude)};

        //adding it to the array
        polylinepath.push(coordinate);
    }

    polyline = new google.maps.Polyline({
        path: polylinepath,
        strokeColor: color,
        strokeOpacity: 1.0,
        strokeWeight: 3.0
    });

    //adding the polyline
    polyline.setMap(map);
}






/*Function that loads the info window on markers takes in argument
the value of i from markers[i] and the json data for stations so that i
could be referenced back to get the lng lat of that station. in json data
its location is i + 1 as data[0] is always the color of the route */
function loadinfo(position, data)
{
    //checking if there is already a InfoWindow opened, if yes then close it
    if(info)
    {
        info.close();
    }

    //ajax call for departures
    var timings = departures(data[position + 1].abbr);
    //console.log(timings.departures.length);

    //formating data into a table
    var contentstring = format_departures(timings);

    console.log("marker clicked on " + data[position + 1 ].name + "( " + data[position + 1].abbr + " )");

    info = new google.maps.InfoWindow({
        content: contentstring
    });

    info.open(map, markers[position]);
}





/*Function to make ajax calls to get departures timinngs*/
function departures(station)
{
    /*AJAX call*/
    var data;
    var xhr;
    try
    {
        xhr = new XMLHttpRequest();
    }
    catch (e)
    {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if(xhr == false)
    {
        alert("javascript not supported");
    }

    var url = "timings.php?station=" + station;

    xhr.onreadystatechange = function ()
    {
        if(xhr.readyState == 4)
        {
            if(xhr.status == 200)
            {
                data = JSON.parse(xhr.responseText);

            }
        }
    }

    xhr.open("GET", url,  false);
    xhr.send(null);

    return data;

}




/*Function that formats the json data that the function loadinfo gets
and formats it into a table */
function format_departures(data)
{
    //if json returned has any data
    if(data["departures"])
    {
        //length of the array
        var length = data["departures"].length;
        console.log("data gotten: " + data);

        var string = "<h1>Departures</h1><table class = \"table table-striped\"><thead><tr><th>Destination</th><th>Time Remaining</th><th>Platform</th></tr></thead>";
        string = string + "<tbody>";


        for(var i = 0; i < length; i++)
        {
            string = string + "<tr>";
            string = string + "<td>";
            string = string + data["departures"][i].destination;
            string = string + "</td>";
            string = string + "<td>";

            if(data["departures"][i].time != "Leaving")
            {
                string = string + data["departures"][i].time + " minutes";
            }
            else
            {
                string = string + data["departures"][i].time;
            }
            string = string + "</td>";
            string = string + "<td>";
            string = string + data["departures"][i].platform;
            string = string + "</td>";
            string = string + "</tr>";
        }

        string = string + "</tbody></table>";

        return string;
    }
    else
    {
        var string = "<p>no trains leaving</p>";
        return string;
    }
}
