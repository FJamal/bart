<?php
    //configuration
    require("../includes/config.php");

    $route = $_GET["routename"];

    /*sql to get all the stations in the route and it is set by order so that the
    plylines make a correct one point to another line */
    $sql = "SELECT {$route}.id, stations, name, longitude, latitude FROM {$route} JOIN stations ON stations = abbr ORDER BY {$route}.id ASC";

    //below commented sql was making a hap hazard polyline 
    //$sql = "SELECT stations, name, longitude, latitude FROM {$route} JOIN stations ON stations = abbr";
    //to get all the stations in the route
    $rows= $dbh->query($sql);

    //getting the color of the route
    $sql2 = $dbh->prepare("SELECT color FROM colors WHERE route = :route");
    $sql2->bindValue(":route", $route);
    $sql2->execute();

    $color = $sql2->fetch(PDO::FETCH_ASSOC);

    $route_stations = [];
    array_push($route_stations, $color);


    foreach($rows as $row)
    {
        $route_stations [] = [
            "name" => $row["name"],
            "abbr" => $row["stations"],
            "longitude" => $row["longitude"],
            "latitude" => $row["latitude"]
        ];
    }

    //outputting as json
    header("Content-type: application/json");
    print(json_encode($route_stations));
?>
