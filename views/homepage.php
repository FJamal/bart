<!DOCTYPE html>
<html>
    <head>
        <!-- Latest compiled and minified CSS bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="/css/style.css">

        <!--loading google maps api-->
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADcLXdwQm2h5Cb08bp9rt027ZjpOjpiDc"></script>


        <meta charset="utf-8">
        <title>Bart</title>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div id = "header">
                        <h1>Bay Area Rapid Transit</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div id = "routes">
                        <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Select Route
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li>Route 1</li>
                                    <li>Route 2</li>
                                    <li>Route 3</li>
                                    <li>Route 4</li>
                                    <li>Route 5</li>
                                    <li>Route 6</li>
                                    <li>Route 7</li>
                                    <li>Route 8</li>
                                    <li>Route 11</li>
                                    <li>Route 12</li>
                                    <li>Route 19</li>
                                    <li>Route 20</li>
                                </ul>
                            </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div id = "map">

                    </div>
                </div>

            </div>
        </div>
    </body>
<script src = "js/mapscript.js"></script>
</html>
